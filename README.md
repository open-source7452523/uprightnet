# Upright-Net: Learning Upright Orientation for 3D Point Cloud

Created by Xufang Pang, Feng Li, Ning Ding and Xiaopin Zhong.

![image](./network.png)

## Citation

If our paper is helpful for your research, please consider citing:
```
@InProceedings{
	Pang_2022_CVPR, 
	author    = {Pang, Xufang and Li, Feng and Ding, Ning and Zhong, Xiaopin}, 
	title     = {Upright-Net: Learning Upright Orientation for 3D Point Cloud}, 
	booktitle = {Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR)}, 
	month     = {June}, 
	year      = {2022}, 
	pages     = {14911-14919} 
}
```

## Introduction

This work is based on our [CVPR'22 paper](https://openaccess.thecvf.com/content/CVPR2022/html/Pang_Upright-Net_Learning_Upright_Orientation_for_3D_Point_Cloud_CVPR_2022_paper.html).  

**Upright-Net** is a novel deep net architecture for estimating the upright orientation of 3D point clouds. We apply a data-driven deep learning method to automatically encode those rules and formulate the upright orientation estimation problem as a classification model, i.e. extract the points on a 3D model that forms the natural base. And then the upright orientation is computed as the normal of the natural base. Results show that our network outperforms previous approaches on orientation estimation and also achieves remarkable generalization capability and transfer capability.

## Requirements
* Ubuntu20.04
* Python3.7
* Pytorch1.5
* CUDA11.1
* Package: numpy, sklearn, tqdm

## Datasets
We generate a data set named as **[UprightNet15](https://github.com/XufangPANG/UprightNet-CVPR2022/tree/main/dataset)**by selecting 15 common object categories with an upright standing pose from [ModelNet40](https://modelnet.cs.princeton.edu/). We annotate points with coordinate y smaller than a threshold (0.05) as supporting points and rotate each model 100 times by uniform random sampled angles to gain a data set covering various possible poses. The data is apportioned into training and test sets with a 3-1 split.

## Usage

### Running the evaluation script with pretrained models:
```
python test.py
```

### Running the the training script:
```
python train.py
```


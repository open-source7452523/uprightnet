import os
from datetime import datetime
import torch
import torch.nn as nn
import torch.utils.data
from torch.utils.data import DataLoader
from tqdm import tqdm
from network import UprightNet
import numpy as np
from sklearn import linear_model
import random
from Common import loss_utils
from Common.RobustPointSetDataLoader import RobustPointSetDataLoader

torch.cuda.current_device()
torch.cuda._initialized = True

class Model:
    def __init__(self, opts):
        self.opts = opts

    def train(self):
        '''DATA LOADING'''
        print('Loading dataset ...')
        trainDataLoader = DataLoader(RobustPointSetDataLoader(self.opts, partition='train'),
                                     batch_size=self.opts.batch_size, shuffle=True)
        testDataLoader = DataLoader(RobustPointSetDataLoader(self.opts, partition='test'),
                                    batch_size=self.opts.batch_size, shuffle=False)

        print("The number of training data is: %d" % len(trainDataLoader.dataset))
        print("The number of testing data is: %d" % len(testDataLoader.dataset))


        '''MODEL LOADING'''
        os.environ["CUDA_VISIBLE_DEVICES"] = self.opts.gpu_idx
        # Create model
        if self.opts.model_name == 'uprightnet':
            segmentor = UprightNet().cuda()
        else:
            raise ValueError('Unknown network: %s' % (self.opts.model_name))

        if torch.cuda.device_count() > 1:
            print("Let's use", torch.cuda.device_count(), "GPUs!")
            segmentor = nn.DataParallel(segmentor)

        # Set random seed for reproducibility
        if self.opts.seed < 0:
            self.opts.seed = random.randint(1, 10000)
        print("Random Seed: %d" % (self.opts.seed))
        random.seed(self.opts.seed)
        torch.manual_seed(self.opts.seed)

        # Create optimizer
        optimizer = torch.optim.Adam(
            segmentor.parameters(),
            lr=self.opts.learning_rate,
            betas=(0.9, 0.999),
            eps=1e-08,
            weight_decay=self.opts.weight_decay
        )
        if self.opts.no_decay:
            scheduler = None
        else:
            scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=self.opts.decay_rate)

        '''TRAINING'''
        print('Start training...')

        for epoch in range(self.opts.epoch):
            print('Epoch %d / %s:' % (epoch + 1, self.opts.epoch))
            if scheduler is not None:
                scheduler.step(epoch)

            train_loss_bce = 0.
            train_loss_fr = 0.

            for _, data in tqdm(enumerate(trainDataLoader, 0), total=len(trainDataLoader), smoothing=0.9):
                points_original, points_rotation, _, _, pid, coef_d = data
                points_original, points_rotation, pid, coef_d = points_original.cuda(), points_rotation.cuda(), pid.cuda(), coef_d.cuda()

                points_rotation = points_rotation.transpose(2, 1).contiguous()
                segmentor = segmentor.train()
                pred = segmentor(points_rotation)
                pred = pred.squeeze()

                optimizer.zero_grad()
                bce_loss = loss_utils.bce_loss(pred, pid)
                fr_loss = loss_utils.fr_loss(pred, points_original, coef_d)
                loss = bce_loss + self.opts.alpha * fr_loss
                train_loss_bce += bce_loss * self.opts.batch_size
                train_loss_fr += fr_loss * self.opts.batch_size
                loss.backward()
                optimizer.step()
            print('BCE Loss: %.3f, FR Loss: %.3f' % (train_loss_bce, train_loss_fr))

        current_time = datetime.now().strftime("%Y%m%d-%H%M")
        torch.save(segmentor.state_dict(), os.path.join(self.opts.model_dir, 'model' + current_time + '.pth'))
        torch.save(optimizer.state_dict(), os.path.join(self.opts.model_dir, 'optimizer' + current_time + '.pth'))

        print('End of training...')
        
        '''TESTING'''
        print('Start testing...')
        orientations = torch.empty((0, 3))
        for _, data in tqdm(enumerate(testDataLoader, 0), total=len(testDataLoader), smoothing=0.9):
            points_original, points_rotation, _, rotm, _, _ = data
            points_original, points_rotation, rotm = points_original.cuda(), points_rotation.cuda(), rotm.cuda()

            points_rotation = points_rotation.transpose(2, 1).contiguous()
            segmentor = segmentor.eval()
            with torch.no_grad():
                test_pred = segmentor(points_rotation)

            orientation = self.UprightOriEst(test_pred.squeeze(), points_original, rotm)
            orientations = torch.cat((orientations, orientation), dim=0)

        angle = torch.acos_(orientations[:,1]) / torch.pi * 180
        me = torch.mean(angle)
        acc = torch.sum(angle < 10) / angle.shape[0] * 100
        print("Mean Error:", me)
        print("Accuracy:", acc)
        print('End of testing...')


    def test(self):
        '''DATA LOADING'''
        print('Loading dataset ...')
        testDataLoader = DataLoader(RobustPointSetDataLoader(self.opts, partition='test'),
                                    batch_size=self.opts.batch_size, shuffle=False)
        print("The number of testing data is: %d" % len(testDataLoader.dataset))

        '''MODEL LOADING'''
        os.environ["CUDA_VISIBLE_DEVICES"] = self.opts.gpu_idx
        # Create model
        if self.opts.model_name == 'uprightnet':
            segmentor = UprightNet().cuda()
        else:
            raise ValueError('Unknown network: %s' % (self.opts.model_name))

        if torch.cuda.device_count() > 1:
            print("Let's use", torch.cuda.device_count(), "GPUs!")
            segmentor = nn.DataParallel(segmentor)
        model_file = os.path.join(self.opts.model_dir, self.opts.model_file)
        segmentor.load_state_dict(torch.load(model_file))

        # Set random seed for reproducibility
        if self.opts.seed < 0:
            self.opts.seed = random.randint(1, 10000)
        print("Random Seed: %d" % (self.opts.seed))
        random.seed(self.opts.seed)
        torch.manual_seed(self.opts.seed)

        '''TESTING'''
        print('Start testing...')
        orientations = torch.empty((0, 3))
        for _, data in tqdm(enumerate(testDataLoader, 0), total=len(testDataLoader), smoothing=0.9):
            points_original, points_rotation, _, rotm, _, _ = data
            points_original, points_rotation, rotm = points_original.cuda(), points_rotation.cuda(), rotm.cuda()

            points_rotation = points_rotation.transpose(2, 1).contiguous()
            segmentor = segmentor.eval()
            with torch.no_grad():
                test_pred = segmentor(points_rotation)

            orientation = self.UprightOriEst(test_pred.squeeze(), points_original, rotm)
            orientations = torch.cat((orientations, orientation), dim=0)

        angle = torch.acos_(orientations[:,1]) / torch.pi * 180
        me = torch.mean(angle)
        acc = torch.sum(angle < 10) / angle.shape[0] * 100
        print("Mean Error:", me)
        print("Accuracy:", acc)
        print('End of testing...')


    def UprightOriEst(self, pred, original, rotm):
        bsize = original.size()[0]
        pred = pred > 0.5
        orientations = torch.empty([0, 3])
        ransac = linear_model.RANSACRegressor(residual_threshold=0.03)
        for i in range(bsize):
            points = original[i]
            mcenter = torch.mean(points, axis=0).cpu()
            spoints = torch.index_select(points, dim=0, index=pred[i, :].nonzero().squeeze()).cpu()   # supporting points
            numspoints = spoints.shape[0]
            # the normal of supporting plane pointing to the mass center
            if numspoints >= 3:
                spoints = spoints
                ransac.fit(spoints[:, [0, 2]], spoints[:, 1])
                a, c = ransac.estimator_.coef_  # coefficients
                d = ransac.estimator_.intercept_  # intercept
                orientation = torch.tensor([a, -1., c]) / torch.norm(torch.tensor([a, -1., c]), p=2) * \
                    torch.sign(a * mcenter[0] - 1.0 * mcenter[1] + c * mcenter[2] + d)
            # the unit vector from center of supporting points to the mass center
            elif numspoints > 0:
                scenter = torch.mean(spoints, axis=0)
                orientation = (mcenter - scenter) / torch.norm(mcenter - scenter, p=2)
            # the fixed output [0, 1, 0] (we use the inversed rotation matrix as output if estimate the original point cloud)
            else:
                # orientation = torch.tensor([0., 1., 0.], dtype=torch.float64)
                orientation = torch.inverse(rotm[i].cpu())[1]
            orientations = torch.cat((orientations, orientation.unsqueeze(0)), dim=0)
        return orientations


